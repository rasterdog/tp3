GDPC                                                                                <   res://.import/ball.png-9a4ca347acb7532f6ae347744a6b04f7.stex 5            �_ubIh�u�Dg�><   res://.import/box.png-196cb2b6d7422a4ab3b9c60b5a994fe3.stex  :            9bViz���%��C١�@   res://.import/tilemap.png-838486f2ee5f5764f6a08fbb86e9df3c.stex  K      �      d��
lu㸤�
gQ   res://Ball.tscn `      +      |z�Ƌ�]%҅
�S�   res://Box.tscn  �      �      �]kD�v޴�ٗ�00   res://TP3.tscn  p      �,      �J$��2��W�Ah3�   res://ball.png.import   �7      �      ����N����}Uj�&   res://box.png.import ;      }      >��y�86T�2�|0�n�   res://default_env.tres  �=      �       um�`�N��<*ỳ�8   res://effect.gd.remap   �Q      !       ��1���1�<b0q   res://effect.gdcP>      �      �h
��� �|>�   res://logic.gd.remap�Q              ��0� p�sj?O���   res://logic.gdc �B      u      A֞��]:��}�s��   res://project.binary�Q      L      C_���}P�������   res://soundwave.shader  pH      �      �R�=����lQ��[   res://tilemap.png.import O      �      )��4䂨vɩQ:m_            [gd_scene load_steps=4 format=2]

[ext_resource path="res://ball.png" type="Texture" id=1]

[sub_resource type="PhysicsMaterial" id=5]
bounce = 0.5

[sub_resource type="CircleShape2D" id=6]
radius = 32.1869

[node name="Ball" type="RigidBody2D" groups=[
"Body",
]]
position = Vector2( 504.699, 334.652 )
collision_layer = 3
physics_material_override = SubResource( 5 )
gravity_scale = 5.0

[node name="CollisionShape2D" type="CollisionShape2D" parent="."]
shape = SubResource( 6 )

[node name="Sprite" type="Sprite" parent="."]
texture = ExtResource( 1 )
     [gd_scene load_steps=3 format=2]

[ext_resource path="res://box.png" type="Texture" id=1]

[sub_resource type="RectangleShape2D" id=7]
extents = Vector2( 32, 32 )

[node name="Box" type="RigidBody2D" groups=[
"Body",
]]
position = Vector2( 635.522, 356.266 )
collision_layer = 3
gravity_scale = 5.0

[node name="CollisionShape2D" type="CollisionShape2D" parent="."]
shape = SubResource( 7 )

[node name="Sprite" type="Sprite" parent="."]
texture = ExtResource( 1 )
               [gd_scene load_steps=12 format=2]

[ext_resource path="res://tilemap.png" type="Texture" id=1]
[ext_resource path="res://Box.tscn" type="PackedScene" id=2]
[ext_resource path="res://Ball.tscn" type="PackedScene" id=3]
[ext_resource path="res://soundwave.shader" type="Shader" id=4]
[ext_resource path="res://effect.gd" type="Script" id=5]
[ext_resource path="res://logic.gd" type="Script" id=6]

[sub_resource type="ConvexPolygonShape2D" id=1]
points = PoolVector2Array( 0, 0, 64, 0, 64, 64, 0, 64 )

[sub_resource type="ConvexPolygonShape2D" id=2]
points = PoolVector2Array( 0, 0, 64, 0, 64, 64, 0, 64 )

[sub_resource type="TileSet" id=3]
0/name = "tilemap.png 0"
0/texture = ExtResource( 1 )
0/tex_offset = Vector2( 0, 0 )
0/modulate = Color( 1, 1, 1, 1 )
0/region = Rect2( 0, 0, 64, 64 )
0/tile_mode = 0
0/occluder_offset = Vector2( 0, 0 )
0/navigation_offset = Vector2( 0, 0 )
0/shapes = [  ]
0/z_index = 0
1/name = "tilemap.png 1"
1/texture = ExtResource( 1 )
1/tex_offset = Vector2( 0, 0 )
1/modulate = Color( 1, 1, 1, 1 )
1/region = Rect2( 64, 0, 64, 64 )
1/tile_mode = 0
1/occluder_offset = Vector2( 0, 0 )
1/navigation_offset = Vector2( 0, 0 )
1/shapes = [  ]
1/z_index = 0
2/name = "tilemap.png 2"
2/texture = ExtResource( 1 )
2/tex_offset = Vector2( 0, 0 )
2/modulate = Color( 1, 1, 1, 1 )
2/region = Rect2( 0, 64, 64, 64 )
2/tile_mode = 0
2/occluder_offset = Vector2( 0, 0 )
2/navigation_offset = Vector2( 0, 0 )
2/shapes = [ {
"autotile_coord": Vector2( 0, 0 ),
"one_way": false,
"one_way_margin": 1.0,
"shape": SubResource( 1 ),
"shape_transform": Transform2D( 1, 0, 0, 1, 0, 0 )
} ]
2/z_index = 0
3/name = "tilemap.png 3"
3/texture = ExtResource( 1 )
3/tex_offset = Vector2( 0, 0 )
3/modulate = Color( 1, 1, 1, 1 )
3/region = Rect2( 64, 64, 64, 64 )
3/tile_mode = 0
3/occluder_offset = Vector2( 0, 0 )
3/navigation_offset = Vector2( 0, 0 )
3/shapes = [ {
"autotile_coord": Vector2( 0, 0 ),
"one_way": false,
"one_way_margin": 1.0,
"shape": SubResource( 2 ),
"shape_transform": Transform2D( 1, 0, 0, 1, 0, 0 )
} ]
3/z_index = 0

[sub_resource type="ShaderMaterial" id=4]
shader = ExtResource( 4 )
shader_param/viewport_size = Vector2( 1920, 1080 )
shader_param/wave_center_position = null
shader_param/wave_radius = 0.0
shader_param/wave_thickness = 0.0

[sub_resource type="CircleShape2D" id=8]
radius = 100.0

[node name="TP3" type="Node2D"]

[node name="TileMap" type="TileMap" parent="."]
tile_set = SubResource( 3 )
cell_quadrant_size = 64
format = 1
tile_data = PoolIntArray( 0, 3, 0, 1, 3, 0, 2, 3, 0, 3, 3, 0, 4, 3, 0, 5, 3, 0, 6, 3, 0, 7, 3, 0, 8, 3, 0, 9, 3, 0, 10, 3, 0, 11, 3, 0, 12, 3, 0, 13, 3, 0, 14, 3, 0, 15, 3, 0, 16, 3, 0, 17, 3, 0, 18, 3, 0, 19, 3, 0, 20, 3, 0, 21, 3, 0, 22, 3, 0, 23, 3, 0, 24, 3, 0, 25, 3, 0, 26, 3, 0, 27, 3, 0, 28, 3, 0, 29, 3, 0, 65536, 3, 0, 65537, 3, 0, 65538, 3, 0, 65539, 3, 0, 65540, 3, 0, 65541, 3, 0, 65542, 3, 0, 65543, 3, 0, 65544, 3, 0, 65545, 3, 0, 65546, 3, 0, 65547, 3, 0, 65548, 3, 0, 65549, 3, 0, 65550, 3, 0, 65551, 3, 0, 65552, 3, 0, 65553, 3, 0, 65554, 3, 0, 65555, 3, 0, 65556, 3, 0, 65557, 3, 0, 65558, 3, 0, 65559, 3, 0, 65560, 3, 0, 65561, 3, 0, 65562, 3, 0, 65563, 3, 0, 65564, 3, 0, 65565, 3, 0, 131072, 3, 0, 131073, 3, 0, 131074, 0, 0, 131075, 0, 0, 131076, 0, 0, 131077, 0, 0, 131078, 0, 0, 131079, 0, 0, 131080, 0, 0, 131081, 0, 0, 131082, 0, 0, 131083, 0, 0, 131084, 0, 0, 131085, 0, 0, 131086, 0, 0, 131087, 0, 0, 131088, 0, 0, 131089, 0, 0, 131090, 0, 0, 131091, 0, 0, 131092, 0, 0, 131093, 0, 0, 131094, 0, 0, 131095, 0, 0, 131096, 0, 0, 131097, 0, 0, 131098, 0, 0, 131099, 0, 0, 131100, 3, 0, 131101, 3, 0, 196608, 3, 0, 196609, 3, 0, 196610, 0, 0, 196611, 0, 0, 196612, 0, 0, 196613, 0, 0, 196614, 0, 0, 196615, 0, 0, 196616, 0, 0, 196617, 0, 0, 196618, 0, 0, 196619, 0, 0, 196620, 0, 0, 196621, 0, 0, 196622, 0, 0, 196623, 0, 0, 196624, 0, 0, 196625, 0, 0, 196626, 0, 0, 196627, 0, 0, 196628, 0, 0, 196629, 0, 0, 196630, 0, 0, 196631, 0, 0, 196632, 0, 0, 196633, 0, 0, 196634, 0, 0, 196635, 0, 0, 196636, 3, 0, 196637, 3, 0, 262144, 3, 0, 262145, 3, 0, 262146, 0, 0, 262147, 0, 0, 262148, 0, 0, 262149, 0, 0, 262150, 0, 0, 262151, 0, 0, 262152, 0, 0, 262153, 0, 0, 262154, 0, 0, 262155, 0, 0, 262156, 0, 0, 262157, 0, 0, 262158, 0, 0, 262159, 0, 0, 262160, 0, 0, 262161, 0, 0, 262162, 0, 0, 262163, 0, 0, 262164, 0, 0, 262165, 0, 0, 262166, 0, 0, 262167, 0, 0, 262168, 0, 0, 262169, 0, 0, 262170, 0, 0, 262171, 0, 0, 262172, 3, 0, 262173, 3, 0, 327680, 3, 0, 327681, 3, 0, 327682, 0, 0, 327683, 0, 0, 327684, 0, 0, 327685, 0, 0, 327686, 0, 0, 327687, 0, 0, 327688, 0, 0, 327689, 0, 0, 327690, 0, 0, 327691, 0, 0, 327692, 0, 0, 327693, 0, 0, 327694, 0, 0, 327695, 0, 0, 327696, 0, 0, 327697, 0, 0, 327698, 0, 0, 327699, 0, 0, 327700, 0, 0, 327701, 0, 0, 327702, 0, 0, 327703, 0, 0, 327704, 0, 0, 327705, 0, 0, 327706, 0, 0, 327707, 0, 0, 327708, 3, 0, 327709, 3, 0, 393216, 3, 0, 393217, 3, 0, 393218, 0, 0, 393219, 0, 0, 393220, 0, 0, 393221, 0, 0, 393222, 0, 0, 393223, 0, 0, 393224, 0, 0, 393225, 0, 0, 393226, 0, 0, 393227, 0, 0, 393228, 0, 0, 393229, 0, 0, 393230, 0, 0, 393231, 0, 0, 393232, 0, 0, 393233, 0, 0, 393234, 0, 0, 393235, 0, 0, 393236, 0, 0, 393237, 0, 0, 393238, 0, 0, 393239, 0, 0, 393240, 0, 0, 393241, 0, 0, 393242, 0, 0, 393243, 0, 0, 393244, 3, 0, 393245, 3, 0, 458752, 3, 0, 458753, 3, 0, 458754, 0, 0, 458755, 0, 0, 458756, 0, 0, 458757, 0, 0, 458758, 0, 0, 458759, 0, 0, 458760, 0, 0, 458761, 0, 0, 458762, 0, 0, 458763, 0, 0, 458764, 0, 0, 458765, 0, 0, 458766, 0, 0, 458767, 0, 0, 458768, 0, 0, 458769, 0, 0, 458770, 0, 0, 458771, 0, 0, 458772, 0, 0, 458773, 0, 0, 458774, 0, 0, 458775, 0, 0, 458776, 0, 0, 458777, 0, 0, 458778, 0, 0, 458779, 0, 0, 458780, 3, 0, 458781, 3, 0, 524288, 3, 0, 524289, 3, 0, 524290, 0, 0, 524291, 0, 0, 524292, 0, 0, 524293, 0, 0, 524294, 0, 0, 524295, 0, 0, 524296, 0, 0, 524297, 0, 0, 524298, 0, 0, 524299, 0, 0, 524300, 0, 0, 524301, 0, 0, 524302, 0, 0, 524303, 0, 0, 524304, 0, 0, 524305, 0, 0, 524306, 0, 0, 524307, 0, 0, 524308, 0, 0, 524309, 0, 0, 524310, 0, 0, 524311, 0, 0, 524312, 0, 0, 524313, 0, 0, 524314, 0, 0, 524315, 0, 0, 524316, 3, 0, 524317, 3, 0, 589824, 3, 0, 589825, 3, 0, 589826, 0, 0, 589827, 0, 0, 589828, 0, 0, 589829, 0, 0, 589830, 0, 0, 589831, 0, 0, 589832, 0, 0, 589833, 0, 0, 589834, 0, 0, 589835, 0, 0, 589836, 0, 0, 589837, 0, 0, 589838, 0, 0, 589839, 0, 0, 589840, 0, 0, 589841, 0, 0, 589842, 0, 0, 589843, 0, 0, 589844, 0, 0, 589845, 0, 0, 589846, 0, 0, 589847, 0, 0, 589848, 0, 0, 589849, 0, 0, 589850, 0, 0, 589851, 0, 0, 589852, 3, 0, 589853, 3, 0, 655360, 3, 0, 655361, 3, 0, 655362, 0, 0, 655363, 0, 0, 655364, 0, 0, 655365, 0, 0, 655366, 0, 0, 655367, 0, 0, 655368, 0, 0, 655369, 0, 0, 655370, 0, 0, 655371, 0, 0, 655372, 0, 0, 655373, 0, 0, 655374, 0, 0, 655375, 0, 0, 655376, 0, 0, 655377, 0, 0, 655378, 0, 0, 655379, 0, 0, 655380, 0, 0, 655381, 0, 0, 655382, 0, 0, 655383, 0, 0, 655384, 0, 0, 655385, 0, 0, 655386, 0, 0, 655387, 0, 0, 655388, 3, 0, 655389, 3, 0, 720896, 3, 0, 720897, 3, 0, 720898, 0, 0, 720899, 0, 0, 720900, 0, 0, 720901, 0, 0, 720902, 0, 0, 720903, 0, 0, 720904, 0, 0, 720905, 0, 0, 720906, 0, 0, 720907, 0, 0, 720908, 0, 0, 720909, 0, 0, 720910, 0, 0, 720911, 0, 0, 720912, 0, 0, 720913, 0, 0, 720914, 0, 0, 720915, 0, 0, 720916, 0, 0, 720917, 0, 0, 720918, 0, 0, 720919, 0, 0, 720920, 0, 0, 720921, 0, 0, 720922, 0, 0, 720923, 0, 0, 720924, 3, 0, 720925, 3, 0, 786432, 3, 0, 786433, 3, 0, 786434, 0, 0, 786435, 0, 0, 786436, 0, 0, 786437, 0, 0, 786438, 0, 0, 786439, 0, 0, 786440, 0, 0, 786441, 0, 0, 786442, 0, 0, 786443, 0, 0, 786444, 0, 0, 786445, 0, 0, 786446, 0, 0, 786447, 0, 0, 786448, 0, 0, 786449, 0, 0, 786450, 0, 0, 786451, 0, 0, 786452, 0, 0, 786453, 0, 0, 786454, 0, 0, 786455, 0, 0, 786456, 0, 0, 786457, 0, 0, 786458, 0, 0, 786459, 0, 0, 786460, 3, 0, 786461, 3, 0, 851968, 3, 0, 851969, 3, 0, 851970, 1, 0, 851971, 1, 0, 851972, 1, 0, 851973, 1, 0, 851974, 1, 0, 851975, 1, 0, 851976, 1, 0, 851977, 1, 0, 851978, 1, 0, 851979, 1, 0, 851980, 1, 0, 851981, 1, 0, 851982, 1, 0, 851983, 1, 0, 851984, 1, 0, 851985, 1, 0, 851986, 1, 0, 851987, 1, 0, 851988, 1, 0, 851989, 1, 0, 851990, 1, 0, 851991, 1, 0, 851992, 1, 0, 851993, 1, 0, 851994, 1, 0, 851995, 1, 0, 851996, 3, 0, 851997, 3, 0, 917504, 3, 0, 917505, 3, 0, 917506, 2, 0, 917507, 2, 0, 917508, 2, 0, 917509, 2, 0, 917510, 2, 0, 917511, 2, 0, 917512, 2, 0, 917513, 2, 0, 917514, 2, 0, 917515, 2, 0, 917516, 2, 0, 917517, 2, 0, 917518, 2, 0, 917519, 2, 0, 917520, 2, 0, 917521, 2, 0, 917522, 2, 0, 917523, 2, 0, 917524, 2, 0, 917525, 2, 0, 917526, 2, 0, 917527, 2, 0, 917528, 2, 0, 917529, 2, 0, 917530, 2, 0, 917531, 2, 0, 917532, 3, 0, 917533, 3, 0, 983040, 3, 0, 983041, 3, 0, 983042, 3, 0, 983043, 3, 0, 983044, 3, 0, 983045, 3, 0, 983046, 3, 0, 983047, 3, 0, 983048, 3, 0, 983049, 3, 0, 983050, 3, 0, 983051, 3, 0, 983052, 3, 0, 983053, 3, 0, 983054, 3, 0, 983055, 3, 0, 983056, 3, 0, 983057, 3, 0, 983058, 3, 0, 983059, 3, 0, 983060, 3, 0, 983061, 3, 0, 983062, 3, 0, 983063, 3, 0, 983064, 3, 0, 983065, 3, 0, 983066, 3, 0, 983067, 3, 0, 983068, 3, 0, 983069, 3, 0, 1048576, 3, 0, 1048577, 3, 0, 1048578, 3, 0, 1048579, 3, 0, 1048580, 3, 0, 1048581, 3, 0, 1048582, 3, 0, 1048583, 3, 0, 1048584, 3, 0, 1048585, 3, 0, 1048586, 3, 0, 1048587, 3, 0, 1048588, 3, 0, 1048589, 3, 0, 1048590, 3, 0, 1048591, 3, 0, 1048592, 3, 0, 1048593, 3, 0, 1048594, 3, 0, 1048595, 3, 0, 1048596, 3, 0, 1048597, 3, 0, 1048598, 3, 0, 1048599, 3, 0, 1048600, 3, 0, 1048601, 3, 0, 1048602, 3, 0, 1048603, 3, 0, 1048604, 3, 0, 1048605, 3, 0 )

[node name="Box" parent="." instance=ExtResource( 2 )]

[node name="Box8" parent="." instance=ExtResource( 2 )]
position = Vector2( 795.903, 455.072 )

[node name="Box9" parent="." instance=ExtResource( 2 )]
position = Vector2( 941.964, 406.385 )

[node name="Box10" parent="." instance=ExtResource( 2 )]
position = Vector2( 1099.48, 528.102 )

[node name="Box11" parent="." instance=ExtResource( 2 )]
position = Vector2( 1099.48, 528.102 )

[node name="Box2" parent="." instance=ExtResource( 2 )]
position = Vector2( 527.851, 530.543 )

[node name="Box3" parent="." instance=ExtResource( 2 )]
position = Vector2( 653.864, 678.036 )

[node name="Box4" parent="." instance=ExtResource( 2 )]
position = Vector2( 829.997, 741.042 )

[node name="Box5" parent="." instance=ExtResource( 2 )]
position = Vector2( 711.143, 771.114 )

[node name="Box6" parent="." instance=ExtResource( 2 )]
position = Vector2( 751.238, 668.012 )

[node name="Box7" parent="." instance=ExtResource( 2 )]
position = Vector2( 701.119, 574.934 )

[node name="Ball" parent="." instance=ExtResource( 3 )]
position = Vector2( 380.118, 436.322 )

[node name="Ball2" parent="." instance=ExtResource( 3 )]
position = Vector2( 332.862, 348.972 )

[node name="Ball3" parent="." instance=ExtResource( 3 )]
position = Vector2( 1325.22, 462.097 )

[node name="BackBufferCopy" type="BackBufferCopy" parent="."]
copy_mode = 2

[node name="WavesFrame" type="ColorRect" parent="."]
material = SubResource( 4 )
margin_right = 1920.0
margin_bottom = 1080.0
color = Color( 0, 0, 0, 1 )
script = ExtResource( 5 )

[node name="Logic" type="Node2D" parent="."]
script = ExtResource( 6 )

[node name="Area2D" type="Area2D" parent="Logic"]

[node name="CollisionShape2D" type="CollisionShape2D" parent="Logic/Area2D"]
shape = SubResource( 8 )
[connection signal="body_entered" from="Logic/Area2D" to="Logic" method="_on_Area2D_body_entered"]
         GDST@   @           c  PNG �PNG

   IHDR   @   @   �iq�  &IDATx��MR� �S^�r��9��� Vy]��0q�	�����fxK+4|��Z�@���U�E��ɝ����$�R��D�pM`�׆��Z7Ǆh��}A`�+ieBp�2x�,v���[�M�`��
�+j�k�EPT< X��T����]�I�)0���VQ���V�	G��������?_E��8��R��xD
(�×�����2�)|+pjψ�	��G�S�Rg���P��O�aq�4=��
��M@����ie��-�V��U7@<�o@��+�_��	P����IU��}�π����2�@�`��0�|F�>0�g�(�f�� 逑�?0�
��@f&����i���=��	����6XYg���!FqJ���M!FN���J��&�b[`$H��O��΁K�Z�) ����^�V�ߡ(<�l����dg��`�g�a�̌��	J��������P'gA�1>|t�2������zz��`�O��Iq�pK@WWgc�V�O�ml�v����ۢlQI`��î�s���C�&_�w�    IEND�B`� [remap]

importer="texture"
type="StreamTexture"
path="res://.import/ball.png-9a4ca347acb7532f6ae347744a6b04f7.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://ball.png"
dest_files=[ "res://.import/ball.png-9a4ca347acb7532f6ae347744a6b04f7.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
GDST@   @             PNG �PNG

   IHDR   @   @   �iq�   �IDATx���!B1 Q �#p��C�;�	@���%쌪��L6���o��k7���l������P -�)�Ќ��.�O�_x|���>~
�4��hM��� Z@S -�)��@h
�4��hM��� Z@S -�)��@h
�4��hM��� Z@S -�`�.����O@��� Z@S -�yp%� [    IEND�B`�  [remap]

importer="texture"
type="StreamTexture"
path="res://.import/box.png-196cb2b6d7422a4ab3b9c60b5a994fe3.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://box.png"
dest_files=[ "res://.import/box.png-196cb2b6d7422a4ab3b9c60b5a994fe3.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
   [gd_resource type="Environment" load_steps=2 format=2]

[sub_resource type="ProceduralSky" id=1]

[resource]
background_mode = 2
background_sky = SubResource( 1 )
             GDSC         !   �      ��������¶��   �������������������嶶��   ���������������   ����������������   ����������Ŷ   �������������Ŷ�   �������Ŷ���   ����׶��   ��������¶��   ����������Ӷ   ��������������Ŷ   ����¶��   ���������������������Ҷ�   ���������Ӷ�   �������������ض�   �����������¶���   �����������������ض�   ϶��   ���Ӷ���   �����������ڶ���   ���������������۶���   d      ,                  
   spawn_wave        wave_center_position      wave_radius       wave_thickness                                                      	      
   %      )      -      1      2      8      C      G      S      d      r      s      y            �      �      �      �      �      �      �      �       �   !   3YY:�  Y:�  �  Y:�  �  YY;�  �  Y;�  �  YY0�  P�  QV�  �  PQ�  �	  PQ�  �
  PQYY0�  PQV�  &P�  T�  P�  QQV�  �  PQ�  ;�  �  PQT�  PQS�  �  T�  �  T�  �  PQT�  T�  �  T�  PQT�  P�  R�  QSYY0�	  PQV�  �  �  �  �  �  �  �  YY0�
  PQV�  T�  PQT�  P�  R�  Q�  T�  PQT�  P�  R�  QYY0�  PQV�  �  �  �  �  �  Y` GDSC      	   &   �      ���ӄ�   ���������������   ����������������   ����������Ŷ   ������������ض��   ������   �����������������   �����϶�   �������Ӷ���   ���������������Ŷ���   ����׶��   ��������¶��   ����������Ӷ   ����¶��   ���������������������Ҷ�   ���������Ӷ�   �����������������ض�   �������ض���   ����Ӷ��   �����Ŷ�   �����������¶���   �������ׄ�������������Ҷ   ���϶���   ����������ƶ   �����������Ķ���   ���������Ҷ�   ������������Ӷ��   ,                     Area2D        Area2D/CollisionShape2D    
   spawn_wave               Body                                                           	   #   
   $      *      1      8      9      @      D      H      I      O      ]      ^      d      e      k      o      u      v      |      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   3YY:�  Y:�  �  YY;�  �  Y;�  �  P�  R�  QY;�  Y;�  YY0�  PQV�  �  �  P�  Q�  �  �  P�  QYY0�	  P�
  QV�  �  PQ�  �  PQYY0�  PQV�  &P�  T�  P�  QQV�  PQ�  Y0�  PQV�  �  �  �  PQ�  �  �  �  �  T�  �  YY0�  PQV�  &P�  	�  QV�  �  �  �  �  T�  T�  �  YY0�  PQV�  .�  PQT�  PQYY0�  P�  QV�  &P�  T�  P�  QQV�  ;�  P�  T�  �  QT�  PQP�  Q�  �  &P�  	�  QV�  T�  P�  P�  R�  QR�  Q`           shader_type canvas_item;

uniform vec2 viewport_size;
uniform vec2 wave_center_position;
uniform float wave_radius;
uniform float wave_thickness;

void fragment() {
	vec2 screen_position = SCREEN_UV * viewport_size;
	
	vec2 variation = screen_position - wave_center_position;
	float gap = length(variation);
	
	if(gap < wave_radius && length(variation) > wave_radius - wave_thickness) {
		
		float a = gap - wave_radius + wave_thickness;
		
		float intensity = a / wave_radius;
		
		vec2 offset = 0.5 * variation / viewport_size * intensity;
		COLOR = texture(SCREEN_TEXTURE, SCREEN_UV - offset) + vec4(intensity) * 0.05;
	}
	else
		COLOR = texture(SCREEN_TEXTURE, SCREEN_UV);
}          GDST�   �           �  PNG �PNG

   IHDR   �   �   �>a�  �IDATx�휱��0E�Y��@џ�ڡC������@Qd��N��q,�2I�s�8O�Iʖ������d������tܟ߷�ǟ����0��3��H���<;�2�R�3J�j���AV��2Q�����&P�A q@�\ n��aЖu����͛���- )�`�8x˃�GIj���-C�K��z�o�K�� q�4�L+!�p�^R��(��%��l�/�0��#�w�� � �9�$z�KA y@��`K3�!�y�!lz3��NrY#] M%`�"�l��͠W A>h�A q�x�c	S�u���y5a��'�2�a`d��mՂzLWK�����>��8&�f{��7jZ��WM��K4�W�G��d��m�D�d�J�l�F�8�#���	ܳ��$��j�%�D���FJ���Ms �}�e�D�7�)<h�0���:�	���"��I'�+F=a�������l#���c["� �'8�؞q���}����02P#�LXey�7�z��� �K�tM ؂ �*#�O���q��=�fM`Ƶ�+Z����B*+1S� �w��b��A{r�sً���W�1�#)K ؁ � �8S)�|�����_?�#�@��;��W��o@;��4��.z����t�����m�3  �<�������(����(A�(#}�g �%� \��� �I/ �\� ��L��Od��I����v�~��r9޻�5�ęJi�
���xe� � �8 �� �@}�Kj~?��~@�K��58��-8�N��J�8 �� � �8�����U ��{ _@�A q@�A q@�A q@�A q��~A�Ղ|�    IEND�B`�[remap]

importer="texture"
type="StreamTexture"
path="res://.import/tilemap.png-838486f2ee5f5764f6a08fbb86e9df3c.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://tilemap.png"
dest_files=[ "res://.import/tilemap.png-838486f2ee5f5764f6a08fbb86e9df3c.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
       [remap]

path="res://effect.gdc"
               [remap]

path="res://logic.gdc"
ECFG      _global_script_classes             _global_script_class_icons             application/config/name         TP3    application/run/main_scene         res://TP3.tscn     display/window/size/width      �     display/window/size/height      8  -   display/window/per_pixel_transparency/allowed         -   display/window/per_pixel_transparency/enabled            display/window/stretch/mode         viewport   display/window/stretch/aspect         keep   input/spawn_wave�              deadzone      ?      events              InputEventMouseButton         resource_local_to_scene           resource_name             device            alt           shift             control           meta          command           button_mask           position              global_position               factor       �?   button_index         pressed           doubleclick           script         7   rendering/quality/intended_usage/framebuffer_allocation          #   rendering/quality/2d/use_pixel_snap         2   rendering/quality/filters/anisotropic_filter_level         3   rendering/quality/filters/use_nearest_mipmap_filter            rendering/quality/filters/msaa         )   rendering/environment/default_environment          res://default_env.tres      GDPC